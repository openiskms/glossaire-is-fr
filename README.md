# Glossaire IS FR

Ce projet est un glossaire des termes et définitions relatifs à la sécurité de l'information.

# A

<dl>
    <dt>Actif</dt>
        <dd>Un élément qui a de la valeur pour les parties prenantes. Un actif peut être tangible (e.g. du matériel, un serveur, un matériel réseau) ou intangible (e.g. des personnes, des données, des informations, des logiciels, des capacités, des fonctions, des services, des marques, des droits d'auteur, des brevets, des propriétés intellectuelles, des images ou de la réputation). La valeur d'un bien est déterminée par les parties prenantes en tenant compte des préoccupations liées à la perte tout au long du cycle de vie du système. Ces préoccupations comprennent, sans s'y limiter, les préoccupations liées à l'activité ou à la mission. | Source : <a href="https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-160v2r1.pdf" target="_blank">NIST SP 800-160 Vol. 2 Rev. 1</a></dd>    
    <dt>Action élémentaire</dt>
        <dd>Action unitaire exécutée par une source de risque sur un bien support dans le cadre d'un scénario opérationnel | Source : <a href="https://cyber.gouv.fr/publications/la-methode-ebios-risk-manager-le-guide" target="_blank">Guide EBIOS RM</a></dd>
    <dt>Action de traitement (des non-conformités / problèmes)</dt>
        <dd>Action visant à réduire la probabilité d'occurrence ou l'impact d'une déviation (e.g. non-conformité, problème). Il en existe 3 types :
            <ul>
                <li>Action corrective : action cherchant à éliminer la cause de la déviation</li>
                <li>Action préventive : action cherchant à empêcher l'occurrence de la déviation</li>
                <li>Action curative : action cherchant à traiter les conséquences de la déviation, sans en traiter les causes</li>
            </ul>
        </dd>
    <dt>Appétence au risque</dt>
        <dd>Importance et type de risque qu'un organisme est prêt à saisir ou à préserver | Source : <a href="https://www.iso.org/obp/ui/fr/#iso:std:iso-iec:27005:ed-4:v1:fr" target="_blank">ISO/IEC 27005 v2022</a></dd>
</dl>
